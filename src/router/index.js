import Vue from "vue";
import Router from "vue-router";
import GMap from "@/components/home/GMap";
import Signup from "@/components/auth/Signup";
import Login from "@/components/auth/Login";
import firebase from "firebase";
import ViewProfile from "@/components/profile/ViewProfile";

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: "/",
      name: "GMap",
      component: GMap,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/signup",
      name: "Signup",
      component: Signup
    },
    {
      path: "/login",
      name: "Login",
      component: Login
    },
    {
      path: "/profile/:id",
      name: "Viewprofile",
      component: ViewProfile
    }
  ]
});

router.beforeEach((to, from, next) => {
  // If chek to see if route require auth
  if (to.matched.some(rec => rec.meta.requiresAuth)) {
    // Check auth state of user
    let user = firebase.auth().currentUser;
    if (user) {
      // User signin proceed to route
      next();
    } else {
      // No user signed in, redirect to login
      next({ name: "Login" });
    }
  } else {
    next();
  }
});

export default router;
