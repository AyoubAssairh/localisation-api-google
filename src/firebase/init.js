import firebase from "firebase";
import firestore from "firebase/firestore";

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyCHjhBLq1DjfCGfQeE5gg_Rtc5Q76-TQMI",
  authDomain: "udemy-geo-ninja-95891.firebaseapp.com",
  databaseURL: "https://udemy-geo-ninja-95891.firebaseio.com",
  projectId: "udemy-geo-ninja-95891",
  storageBucket: "udemy-geo-ninja-95891.appspot.com",
  messagingSenderId: "529658589598",
  appId: "1:529658589598:web:b878b39492f23bb050268f",
  measurementId: "G-6TZH1F5MNE"
};
// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig);
firebaseApp.firestore();
firebase.analytics();

export default firebaseApp.firestore();
